# Roadmap

## Issues

See the issues on the project's page for a list. Contributions are welcome!

## Coming next (already done)

### Added

- Added base development tools to get started
- Task creation
- Tasks graphs

### Changed

### Improved

### Removed

### Fixed

## Planned

Note that the elements in this list may change during development; it' a
general idea of what will be done next.

- Handle account destruction
  - Anonymize tasks and comments
  - Destroy all other content
  - Tests

- Create a personal todo list from tasks
  - User can add a task to its todo list
  - User can mark tasks as done

- Create a discussion area per task
  - Users can start new subjects
  - Users can answer to subjects

- Evaluate tasks
  - Users can _vote_ for some values: _time_ to perform task, _price_ to perform task, _felt emergency_
  - Display an average of each values on the tasks; that will help in prioritization
