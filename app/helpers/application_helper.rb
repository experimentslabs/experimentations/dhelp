module ApplicationHelper
  def authored?(entity, user: nil, user_id: nil)
    id = user_id || user || current_user&.id
    entity.user_id == id
  end
end
