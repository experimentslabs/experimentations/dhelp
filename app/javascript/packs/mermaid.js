import Mermaid from 'mermaid'

window.renderMermaidGraph = function (sourceID, targetID) {
  const content = document.getElementById(sourceID).innerText
  const target = document.getElementById(targetID)
  Mermaid.render(`${targetID}-prerender`, content, (svgGraph) => {
    target.innerHTML = svgGraph
    target.classList.add('mermaid__graph--processed')
  })
}

Mermaid.parseError = function (err, hash) {
  console.error(err, hash)
}

Mermaid.initialize({
  startOnLoad: false,
  flowchart: {
    curve: 'basis',
  },
  theme: null, // Use app/assets/lib/mermaid/theme.scss
})
