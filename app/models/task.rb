require 'classes/siblings_extractor'

class Task < ApplicationRecord
  validates :title, presence: true
  validates :description, presence: true
  validates :user_id, presence: true

  belongs_to :user
  # Ancestors are TasksOrders with child equal to current task
  has_many :ancestors_order, class_name: 'TaskOrder', foreign_key: :child_id, inverse_of: :child, dependent: :destroy
  # Children are TasksOrders with ancestor equal to current task
  has_many :children_order, class_name: 'TaskOrder', foreign_key: :ancestor_id, inverse_of: :ancestor, dependent: :destroy
  has_many :ancestors, class_name: 'Task', through: :ancestors_order
  has_many :children, class_name: 'Task', through: :children_order

  before_validation :extract_siblings
  after_create :link_siblings

  def add_child(task)
    children.push task_or_id(task)
  end

  def add_ancestor(task)
    ancestors.push task_or_id(task)
  end

  def full_graph
    extractor = SiblingsExtractor.new self

    {
      siblings: extractor.siblings,
      orders:   extractor.orders,
    }
  end

  private

  def task_or_id(element)
    if element.is_a? Integer
      Task.find element
    elsif element.is_a? Task
      element
    else
      raise "This is not a task nor an ID (#{element.class})"
    end
  end

  # Used before validation to remove siblings because current task has no ID before
  # creation. @siblings is then used to create ancestors and children to the task
  def extract_siblings
    return if id

    @siblings         = { ancestors: ancestor_ids, children: child_ids }
    self.ancestor_ids = []
    self.child_ids    = []
  end

  def link_siblings
    @siblings[:ancestors].each { |id| add_ancestor id }
    @siblings[:children].each { |id| add_child id }
  end
end
