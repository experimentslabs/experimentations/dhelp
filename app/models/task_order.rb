class TaskOrder < ApplicationRecord
  belongs_to :ancestor, class_name: 'Task', inverse_of: :ancestors
  belongs_to :child, class_name: 'Task', inverse_of: :children
end
