class CreateTaskOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :task_orders do |t| # rubocop:disable Rails/CreateTableWithTimestamps
      t.references :ancestor, null: true, foreign_key: { to_table: :tasks }
      t.references :child, null: true, foreign_key: { to_table: :tasks }
    end
  end
end
