# Put your development seeds here
FactoryBot.create :user_known

task_a = FactoryBot.create :task, title: 'Task A'
task_b = FactoryBot.create :task, title: 'Task B'
task_c = FactoryBot.create :task, title: 'Task C'
task_d = FactoryBot.create :task, title: 'Task D', ancestor_ids: [task_a.id, task_b.id, task_c.id]
# task_e
FactoryBot.create :task, title: 'Task E', ancestor_ids: [task_d.id]
task_f = FactoryBot.create :task, title: 'Task F', ancestor_ids: [task_d.id]
task_g = FactoryBot.create :task, title: 'Task G', ancestor_ids: [task_d.id]
# task_h
FactoryBot.create :task, title: 'Task H', ancestor_ids: [task_g.id, task_f.id]
