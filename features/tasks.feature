Feature: Things management
  As a registered member
  I want to access tasks
  An I want to manage my tasks

  Background:
    Given I have an account for "john@doe.com"
    And I'm logged in with this account

  Scenario: List tasks
    Given there are 3 tasks in the system
    When I access the tasks list
    Then I see 3 tasks


  Scenario: Create a task
    Given I access the tasks creation form
    And I create a task to "Learn how to draw people"
    Then I see that task in the tasks list

  Scenario: Show a task
    Given there is a task to "Build a house"
    When I display this task
    Then I see the task details

  Scenario: Edit a task
    Given I created a task to "Eat snaks"
    When I access this task edition form
    And I update this task to "Eat snacks"
    Then the task is updated

  Scenario: Destroy a task
    Given I created a task to "Eat snaks"
    When I destroy this task
    Then this task is not visible in the tasks list
