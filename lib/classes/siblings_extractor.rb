class SiblingsExtractor
  def initialize(task)
    raise 'This is not a task' unless task.is_a? Task

    @task = task
  end

  def siblings
    extract unless @siblings

    @siblings
  end

  def orders
    extract unless @orders

    @orders
  end

  private

  def extract
    @processed_orders = []
    @processed        = []
    @siblings         = []
    @orders           = []

    recurse @task, types: [:ancestors, :children]
  end

  def recurse(task, types: [])
    @processed << task.id
    @siblings << task
    select_orders task, types

    types.each do |type|
      task.send(type).each do |sibling|
        next if @processed.include? sibling.id

        recurse(sibling, types: [type])
      end
    end
  end

  def select_orders(task, types)
    types.each do |t|
      task.send("#{t}_order").each do |order|
        next if @processed_orders.include? order.id

        @processed_orders << order.id
        @orders << order
      end
    end
  end
end
