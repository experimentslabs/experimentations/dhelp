require 'rails_helper'
require 'classes/siblings_extractor'

RSpec.describe SiblingsExtractor do
  let(:unrelated_tasks) { FactoryBot.create_list :task, 5 }
  let(:extractor) { described_class.new task }
  let(:siblings) { extractor.siblings }
  let(:orders) { extractor.orders }

  before do
    unrelated_tasks
  end

  # C -> B -> A
  context 'with a straight ancestry line' do
    let(:distant_ancestor) { FactoryBot.create :task }
    let(:direct_ancestor) { FactoryBot.create :task, ancestor_ids: [distant_ancestor.id] }
    let(:task) { FactoryBot.create :task, ancestor_ids: [direct_ancestor.id] }

    it 'returns 3 siblings' do
      expect(siblings.count).to eq 3
    end

    it 'returns 2 orders' do
      expect(orders.count).to eq 2
    end
  end

  # A -> B -> C
  context 'with a straight children line' do
    let(:distant_child) { FactoryBot.create :task }
    let(:direct_child) { FactoryBot.create :task, child_ids: [distant_child.id] }
    let(:task) { FactoryBot.create :task, child_ids: [direct_child.id] }

    it 'returns 3 siblings' do
      expect(siblings.count).to eq 3
    end

    it 'returns 2 orders' do
      expect(orders.count).to eq 2
    end
  end

  # C -> B -> A -> C
  context 'with a looping ancestry line' do
    let(:distant_ancestor) { FactoryBot.create :task }
    let(:direct_ancestor) { FactoryBot.create :task, ancestor_ids: [distant_ancestor.id] }
    let(:task) { FactoryBot.create :task, ancestor_ids: [direct_ancestor.id], child_ids: [distant_ancestor.id] }

    it 'returns 3 siblings' do
      expect(siblings.count).to eq 3
    end

    it 'returns 3 orders' do
      expect(orders.count).to eq 3
    end
  end

  # A --\   /-> E
  # B --> D --> F --> H
  # C --/   \-> G --/
  context 'with a complex ancestry graph' do
    let(:task_a) { FactoryBot.create :task }
    let(:task_b) { FactoryBot.create :task }
    let(:task_c) { FactoryBot.create :task }
    let(:task_d) { FactoryBot.create :task, ancestor_ids: [task_a.id, task_b.id, task_c.id] }
    let(:task_e) { FactoryBot.create :task, ancestor_ids: [task_d.id] }
    let(:task_f) { FactoryBot.create :task, ancestor_ids: [task_d.id] }
    let(:task_g) { FactoryBot.create :task, ancestor_ids: [task_d.id] }
    let(:task_h) { FactoryBot.create :task, ancestor_ids: [task_g.id, task_f.id] }
    let(:reload_tasks) do
      task_a.reload
      task_b.reload
      task_c.reload
      task_d.reload
      task_e.reload
      task_f.reload
      task_g.reload
      task_h.reload
    end

    before do
      task_h
      reload_tasks
    end

    context 'with the "H" node' do
      let(:extractor) { described_class.new task_h }

      it 'returns 7 siblings' do
        expect(siblings.count).to eq 7
      end

      it 'returns 7 orders' do
        expect(orders.count).to eq 7
      end
    end

    context 'with the "D" node' do
      let(:extractor) { described_class.new task_d }

      it 'returns 8 siblings for D' do
        expect(siblings.count).to eq 8
      end

      it 'returns 8 orders for D' do
        expect(orders.count).to eq 8
      end
    end

    context 'with the "E" node' do
      let(:extractor) { described_class.new task_e }

      it 'returns 5 siblings for E' do
        expect(siblings.count).to eq 5
      end

      it 'returns 4 orders for E' do
        expect(orders.count).to eq 4
      end
    end

    context 'with the "A" node' do
      let(:extractor) { described_class.new task_a }

      it 'returns 6 siblings for A' do
        expect(siblings.count).to eq 6
      end

      it 'returns 6 orders for A' do
        expect(orders.count).to eq 6
      end
    end
  end
end
