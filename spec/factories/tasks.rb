FactoryBot.define do
  factory :task do
    title { Faker::Lorem.sentence word_count: 5 }
    description { Faker::Lorem.paragraphs(number: 2).join("\n") }
    user
  end
end
