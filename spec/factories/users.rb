FactoryBot.define do
  factory :user do
    email { Faker::Internet.unique.email }
    password { 'password' }
    username { Faker::Internet.unique.username }

    trait :active do
      confirmed_at { Time.current }
    end

    factory :user_known do
      email { 'user@example.com' }
      active
    end
  end
end
