require 'rails_helper'

RSpec.describe Task, type: :model do
  let(:unrelated_tasks) { FactoryBot.create_list :task, 5 }
  let(:ancestor) { FactoryBot.create :task }
  let(:task) { FactoryBot.create :task }
  let(:child) { FactoryBot.create :task }

  before do
    unrelated_tasks
  end

  it 'can have ancestors' do
    task.add_ancestor ancestor
    expect(task.ancestors.first.id).to eq ancestor.id
  end

  it 'can have children' do
    task.add_child child
    expect(task.children.first.id).to eq child.id
  end

  context 'when creating a task' do
    let(:task) { FactoryBot.build :task }
    let(:siblings_ids) { FactoryBot.create_list(:task, 2).map(&:id) }

    it 'creates ancestors on creation' do
      task.ancestor_ids = siblings_ids
      task.save
      expect(task.ancestors.count).to eq 2
    end

    it 'creates children on creation' do
      task.child_ids = siblings_ids
      task.save
      expect(task.children.count).to eq 2
    end
  end
end
