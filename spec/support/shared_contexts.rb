RSpec.shared_context 'with authenticated user', shared_context: :metadata do
  before do
    sign_in User.find_by(email: 'user@example.com')
  end
end
